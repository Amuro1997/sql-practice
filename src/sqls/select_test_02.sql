/* 
 * 请告诉我 `employeeNumber` 最大的 employee 的如下信息：
 *
 * +─────────────────+────────────+───────────+
 * | employeeNumber  | firstName  | lastName  |
 * +─────────────────+────────────+───────────+
 */

 select E.employeeNumber, E.firstName, E.lastName from employees E order by E.employeeNumber DESC LIMIT 1;
