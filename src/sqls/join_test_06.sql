/*
 * 请告诉我所有的员工（employee）的姓名及其管理者的姓名。所有的姓名都需要按照 `lastName, firstName`
 * 的格式输出，例如 'Bow, Anthony'。如果员工没有管理者，则其管理者的姓名输出为 '(Top Manager)'。
 * 输出需要包含如下信息：
 *
 * +───────────+──────────+
 * | employee  | manager  |
 * +───────────+──────────+
 *
 * 输出结果按照 `manager` 排序，然后按照 `employee` 排序。
 */


select concat(E1.lastName, ', ', E1.firstName) as employee,
IFNULL(concat(E2.lastName,', ', E2.firstName) , '(Top Manager)') as manager
from employees E1 left JOIN employees E2 on E1.reportsTo = E2.employeeNumber
ORDER BY manager, employee;