/*
 * 请告诉我所有 officeCode 为 1 的职位（jobtitle）为 'Sales Rep' 的员工（employee）信息。
 * 结果应当按照 employeeNumber 排序。结果应当包含如下的信息：
 *
 * +───────────+────────────+───────────+
 * | lastName  | firstName  | jobTitle  |
 * +───────────+────────────+───────────+
 */

select E.lastName, E.firstName, E.jobTitle from employees E where E.officeCode = 1 and E.jobTitle = 'Sales Rep' order by E.employeeNumber;