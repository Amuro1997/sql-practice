/*
 * 请告诉我所有被取消（'Cancelled'）的订单以及其总金额。查询结果应当包含如下的内容：
 *
 * +──────────────+─────────────+───────────────+
 * | orderNumber  | totalPrice  | detailsCount  |
 * +──────────────+─────────────+───────────────+
 *
 * 其中，orderNumber 是订单编号，totalPrice 是订单的总金额而 detailsCount 是每一个订单
 * 包含的 `orderdetails` 的数目。
 *
 * 结果应当按照 `orderNumber` 排序。
 */

select O.orderNumber, sum(od.quantityOrdered * od.priceEach) as 
totalPrice, count(*) as detailsCount 
from orders O, orderdetails od 
where O.status='Cancelled' and O.orderNumber = od.orderNumber 
GROUP by O.orderNumber;